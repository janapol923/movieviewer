package com.insta.japt.movieviewer.di

import com.insta.japt.movieviewer.ui.details.DetailsModule
import com.insta.japt.movieviewer.ui.details.MainActivity
import com.insta.japt.movieviewer.ui.seatMap.SeatMapActivity
import com.insta.japt.movieviewer.ui.seatMap.SeatMapModule
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityBuilder {

    @ContributesAndroidInjector(modules = [DetailsModule::class])
    internal abstract fun bindDetailsActivity(): MainActivity

    @ContributesAndroidInjector(modules = [SeatMapModule::class])
    internal abstract fun bindSeatMapActivity(): SeatMapActivity
}