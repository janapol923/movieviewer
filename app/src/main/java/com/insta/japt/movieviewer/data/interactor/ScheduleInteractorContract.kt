package com.insta.japt.movieviewer.data.interactor

import com.insta.japt.movieviewer.data.model.MovieScheduleResponse
import io.reactivex.disposables.Disposable

interface ScheduleInteractorContract {
    interface OnLoadScheduleDetailsListener {

        fun onScheduleDetailsLoadSuccess(response: MovieScheduleResponse)

        fun onError(error: String)

        fun onFailed()
    }

    fun requestScheduleDetails(listener: OnLoadScheduleDetailsListener): Disposable
}