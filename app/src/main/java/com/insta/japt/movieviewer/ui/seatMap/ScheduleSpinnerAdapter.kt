package com.insta.japt.movieviewer.ui.seatMap

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.TextView
import com.insta.japt.movieviewer.R
import com.insta.japt.movieviewer.data.model.SpinnerModel

class ScheduleSpinnerAdapter(
    context: Context,
    private val layoutResource: Int,
    private val textViewResourceId: Int = 0,
    private val values: List<SpinnerModel>,
    private val type: String
) : ArrayAdapter<SpinnerModel>(context, layoutResource, textViewResourceId, values) {

    override fun getItem(position: Int): SpinnerModel = values[position]

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val view: TextView = createViewFromResource(convertView, parent, layoutResource)
        return bindData(values[position], view)
    }

    override fun getDropDownView(position: Int, convView: View?, parent: ViewGroup): View {
        val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val spinnerItem = inflater.inflate(layoutResource, parent, false)

        val label: TextView = spinnerItem.findViewById(R.id.spinner_item)
        label.text = values[position].label
        label.tag = values[position].id + "," + type
        return spinnerItem
    }

    private fun createViewFromResource(
        convertView: View?,
        parent: ViewGroup,
        layoutResource: Int
    ): TextView {
        val view =
            convertView ?: LayoutInflater.from(context).inflate(layoutResource, parent, false)
        return try {
            if (textViewResourceId == 0) view as TextView
            else {
                view.findViewById(textViewResourceId) ?: throw RuntimeException(
                    "Failed to find TextView"
                )
            }
        } catch (exception: ClassCastException) {
            Log.e("CustomArrayAdapter", "You must supply a resource ID for a TextView")
            throw IllegalStateException(
                "ArrayAdapter requires the resource ID to be a TextView", exception
            )
        }
    }

    private fun bindData(value: SpinnerModel, view: TextView): TextView {
        view.text = value.label
        view.tag = value.id + "," + type
        return view
    }

}