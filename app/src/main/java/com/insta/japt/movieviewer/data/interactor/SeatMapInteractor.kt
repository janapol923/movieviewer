package com.insta.japt.movieviewer.data.interactor

import com.insta.japt.movieviewer.data.model.MovieSeatMapResponse
import com.insta.japt.movieviewer.data.rest.MovieViewerService
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.observers.DisposableSingleObserver
import io.reactivex.schedulers.Schedulers

class SeatMapInteractor(private val service: MovieViewerService) : SeatMapInteractorContract {

    override fun requestSeatMapDetails(listener: SeatMapInteractorContract.OnLoadSeatMapDetailsListener): Disposable {
        val single = service.getMovieSeatMapDetails()
        val singleCached = single.cache()

        return singleCached.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeWith(object : DisposableSingleObserver<MovieSeatMapResponse>() {

                override fun onSuccess(response: MovieSeatMapResponse) {
                    listener.onSeatMapDetailsLoadSuccess(response)
                }

                override fun onError(e: Throwable) {
                    listener.onFailed()
                }
            })
    }

}