package com.insta.japt.movieviewer.ui.details

import android.content.Context
import com.example.shop.timekeepingapplication.util.DateFormatter
import com.insta.japt.movieviewer.R
import com.insta.japt.movieviewer.data.interactor.DetailsInteractorContract
import com.insta.japt.movieviewer.data.model.MovieDetailsResponse
import io.reactivex.disposables.CompositeDisposable

class DetailsPresenter(
    var context: Context,
    var view: DetailsContract.View,
    var interactor: DetailsInteractorContract,
    var disposables: CompositeDisposable
) :
    DetailsContract.Presenter, DetailsInteractorContract.OnDetailsLoadedListener {

    override fun requestMovieDetails() {
        disposables.add(interactor.requestMovieDetails(this))
    }

    override fun onDestroy() {
        disposables.clear()
    }

    override fun onDetailsLoadSuccess(response: MovieDetailsResponse) {
        view.displayMovieDetails(
            response.posterLandscape,
            response.poster,
            response.canonicalTitle,
            response.genre,
            response.advisoryRating,
            getMovieDuration(response.runtimeMins.toInt()),
            DateFormatter.movieDisplayDate(response.releaseDate),
            response.synopsis,
            response.cast
        )
    }

    override fun onError(error: String) {
        view.displayErrorMessage()
    }

    override fun onFailed() {
        view.displayErrorMessage()
    }

    private fun getMovieDuration(runTime: Int): String {
        val hr: Int = runTime / 60
        val mins: Int = runTime - (hr * 60)

        return when {
            mins > 1 -> String.format(
                context.getString(R.string.duration_hr_min_format),
                hr,
                mins
            )
            hr < 1 -> String.format(
                context.getString(R.string.duration_min_format),
                hr
            )
            else -> String.format(
                context.getString(R.string.duration_hr_format),
                hr
            )
        }
    }
}