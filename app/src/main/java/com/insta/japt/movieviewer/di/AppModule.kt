package com.insta.japt.movieviewer.di

import android.content.Context
import com.insta.japt.movieviewer.MovieViewerApplication
import com.insta.japt.movieviewer.data.rest.MovieViewerService
import com.insta.japt.movieviewer.data.rest.RetrofitClient
import dagger.Module
import dagger.Provides
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Named
import javax.inject.Singleton

@Module
class AppModule {

    @Provides
    @Named("ApplicationContext")
    fun provideContext(application: MovieViewerApplication): Context = application

    @Provides
    @Singleton
    fun provideMovieViewerService(@Named("ApplicationContext") context: Context):
            MovieViewerService = RetrofitClient.getMovieViewerService(context)

    @Provides
    fun provideCompositeDisposable(): CompositeDisposable = CompositeDisposable()
}