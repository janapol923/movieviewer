package com.insta.japt.movieviewer.ui.seatMap

import android.content.Context
import com.insta.japt.movieviewer.data.interactor.ScheduleInteractor
import com.insta.japt.movieviewer.data.interactor.ScheduleInteractorContract
import com.insta.japt.movieviewer.data.interactor.SeatMapInteractor
import com.insta.japt.movieviewer.data.interactor.SeatMapInteractorContract
import com.insta.japt.movieviewer.data.rest.MovieViewerService
import dagger.Module
import dagger.Provides
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Named

@Module
class SeatMapModule {

    @Provides
    @Named("ActivityContext")
    fun provideContext(activity: SeatMapActivity): Context = activity

    @Provides
    fun provideSeatMapView(activity: SeatMapActivity): SeatMapContract.View = activity

    @Provides
    fun provideSeatInteractor(service: MovieViewerService):
            SeatMapInteractorContract = SeatMapInteractor(service)

    @Provides
    fun provideScheduleInteractor(service: MovieViewerService):
            ScheduleInteractorContract = ScheduleInteractor(service)

    @Provides
    fun provideSeatMapPresenter(
        @Named("ActivityContext")
        context: Context,
        view: SeatMapContract.View,
        seatMapInteractor: SeatMapInteractorContract,
        scheduleInteractor: ScheduleInteractorContract,
        disposable: CompositeDisposable
    ): SeatMapContract.Presenter =
        SeatMapPresenter(context, view, seatMapInteractor, scheduleInteractor, disposable)

}