package com.insta.japt.movieviewer.di

import com.insta.japt.movieviewer.MovieViewerApplication
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Singleton
@Component(modules = [AndroidSupportInjectionModule::class, AppModule::class, ActivityBuilder::class])
interface AppComponent : AndroidInjector<MovieViewerApplication> {

    @Component.Builder
    abstract class Builder : AndroidInjector.Builder<MovieViewerApplication>()

}