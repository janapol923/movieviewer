package com.insta.japt.movieviewer.data.model

import com.google.gson.annotations.SerializedName

data class MovieSeatMapResponse(
    val seatmap: List<List<String>>,
    val available: Available
)

data class Available(
    val seats: List<String>,
    @SerializedName("seat_count")
    val seatCount: Int
)