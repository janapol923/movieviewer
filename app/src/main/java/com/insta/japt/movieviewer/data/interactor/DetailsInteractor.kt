package com.insta.japt.movieviewer.data.interactor

import com.insta.japt.movieviewer.data.model.MovieDetailsResponse
import com.insta.japt.movieviewer.data.rest.MovieViewerService
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.observers.DisposableSingleObserver
import io.reactivex.schedulers.Schedulers

class DetailsInteractor(private val service: MovieViewerService) : DetailsInteractorContract {

    override fun requestMovieDetails(listener: DetailsInteractorContract.OnDetailsLoadedListener): Disposable {
        val single = service.getMovieDetails()
        val singleCached = single.cache()

        return singleCached.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeWith(object : DisposableSingleObserver<MovieDetailsResponse>() {

                override fun onSuccess(response: MovieDetailsResponse) {
                    listener.onDetailsLoadSuccess(response)
                }

                override fun onError(error: Throwable) {
                    listener.onFailed()
                }
            })
    }

}