package com.example.shop.timekeepingapplication.util

import android.util.Log
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

class DateFormatter {

    companion object {
        fun movieDisplayDate(dateToFormat: String): String {
            val prevFormat = SimpleDateFormat("yyyy-MM-dd", Locale.US)
            val parsedDate = try {
                prevFormat.parse(dateToFormat)
            } catch (exception: ParseException) {
                Log.e("LOG", "ERROR: $exception")
                Calendar.getInstance().time
            }
            val newFormat = SimpleDateFormat("MMMM dd, yyyy", Locale.US)
            return newFormat.format(parsedDate)
        }
    }
}