package com.insta.japt.movieviewer.ui.seatMap

import android.content.Context
import com.insta.japt.movieviewer.R
import com.insta.japt.movieviewer.data.interactor.ScheduleInteractorContract
import com.insta.japt.movieviewer.data.interactor.SeatMapInteractorContract
import com.insta.japt.movieviewer.data.model.MovieScheduleResponse
import com.insta.japt.movieviewer.data.model.MovieSeatMapResponse
import io.reactivex.disposables.CompositeDisposable

class SeatMapPresenter(
    val context: Context,
    val view: SeatMapContract.View,
    val seatMapInteractor: SeatMapInteractorContract,
    val scheduleInteractor: ScheduleInteractorContract,
    val disposables: CompositeDisposable
) : SeatMapContract.Presenter,
    SeatMapInteractorContract.OnLoadSeatMapDetailsListener,
    ScheduleInteractorContract.OnLoadScheduleDetailsListener {

    private var mPricePerSeat: Float = 0f
    private var mSelectedSeats: Int = 0

    override fun requestSeatMapDetails() {
        disposables.add(seatMapInteractor.requestSeatMapDetails(this))
    }

    override fun requestScheduleDetails() {
        disposables.add(scheduleInteractor.requestScheduleDetails(this))
    }

    override fun updatePricePerSeat(price: Float) {
        mPricePerSeat = price
        displayTotalPrice()
    }

    override fun addSeat() {
        mSelectedSeats += 1
        displayTotalPrice()
    }

    override fun onDestroy() {
        disposables.clear()
    }

    override fun onSeatMapDetailsLoadSuccess(response: MovieSeatMapResponse) {
        view.displaySeatMapDetails(response.seatmap, response.available)
    }

    override fun onScheduleDetailsLoadSuccess(response: MovieScheduleResponse) {
        view.displayScheduleDetails(response.dates, response.cinemas, response.times)
    }

    override fun onError(error: String) {
        view.displayErrorMessage()
    }

    override fun onFailed() {
        view.displayErrorMessage()
    }

    private fun displayTotalPrice() {
        view.displayTotalPrice(
            String.format(
                context.getString(R.string.price_format),
                mPricePerSeat * mSelectedSeats
            )
        )
    }

}