package com.insta.japt.movieviewer.data.interactor

import com.insta.japt.movieviewer.data.model.MovieSeatMapResponse
import io.reactivex.disposables.Disposable

interface SeatMapInteractorContract {
    interface OnLoadSeatMapDetailsListener {

        fun onSeatMapDetailsLoadSuccess(response: MovieSeatMapResponse)

        fun onError(error: String)

        fun onFailed()
    }

    fun requestSeatMapDetails(listener: OnLoadSeatMapDetailsListener): Disposable
}