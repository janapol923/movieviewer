package com.insta.japt.movieviewer.ui.details

import android.content.Context
import com.insta.japt.movieviewer.data.interactor.DetailsInteractor
import com.insta.japt.movieviewer.data.interactor.DetailsInteractorContract
import com.insta.japt.movieviewer.data.rest.MovieViewerService
import dagger.Module
import dagger.Provides
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Named

@Module
class DetailsModule {

    @Provides
    @Named("ActivityContext")
    fun provideContext(activity: MainActivity): Context = activity

    @Provides
    fun provideDetailsView(activity: MainActivity): DetailsContract.View = activity

    @Provides
    fun provideDetailsInteractor(service: MovieViewerService):
            DetailsInteractorContract = DetailsInteractor(service)

    @Provides
    fun provideDetailsPresenter(
        @Named("ActivityContext")
        context: Context,
        view: DetailsContract.View,
        interactor: DetailsInteractorContract,
        disposables: CompositeDisposable
    ): DetailsContract.Presenter = DetailsPresenter(context, view, interactor, disposables)
}