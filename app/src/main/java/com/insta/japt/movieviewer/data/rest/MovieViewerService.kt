package com.insta.japt.movieviewer.data.rest

import com.insta.japt.movieviewer.data.model.MovieDetailsResponse
import com.insta.japt.movieviewer.data.model.MovieScheduleResponse
import com.insta.japt.movieviewer.data.model.MovieSeatMapResponse
import io.reactivex.Single
import retrofit2.http.GET


@JvmSuppressWildcards
interface MovieViewerService {

    @GET("/movie.json")
    fun getMovieDetails(): Single<MovieDetailsResponse>

    @GET("/seatmap.json")
    fun getMovieSeatMapDetails(): Single<MovieSeatMapResponse>

    @GET("/schedule.json")
    fun getMovieScheduleDetails(): Single<MovieScheduleResponse>
}