package com.insta.japt.movieviewer.ui.details

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.insta.japt.movieviewer.R
import kotlinx.android.synthetic.main.layout_cast.view.*

class CastAdapter(private val cast: List<String>) : RecyclerView.Adapter<CastAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val itemView =
            LayoutInflater.from(parent.context).inflate(R.layout.layout_cast, parent, false)
        return ViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.castTextView.text = cast[position]
    }

    override fun getItemCount(): Int {
        return cast.size
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val castTextView: TextView = itemView.movie_cast
    }
}