package com.insta.japt.movieviewer.ui.details

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import com.insta.japt.movieviewer.R
import com.insta.japt.movieviewer.ui.seatMap.SeatMapActivity
import com.squareup.picasso.Picasso
import dagger.android.support.DaggerAppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*
import javax.inject.Inject

class MainActivity : DaggerAppCompatActivity(), DetailsContract.View {

    @Inject
    lateinit var mPresenter: DetailsContract.Presenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        loader_view.visibility = View.VISIBLE
        mPresenter.requestMovieDetails()

        seat_map_button_tv.setOnClickListener {
            startActivity(Intent(this, SeatMapActivity::class.java))
        }
    }

    override fun onDestroy() {
        mPresenter.onDestroy()
        super.onDestroy()
    }

    override fun displayMovieDetails(
        backdropUrl: String,
        posterUrl: String,
        name: String,
        genre: String,
        rating: String,
        duration: String,
        releaseDate: String,
        synopsis: String,
        cast: List<String>
    ) {
        Picasso.get().load(backdropUrl).into(backdrop_iv)
        Picasso.get().load(posterUrl).into(poster_iv)

        movie_name_tv.text = name
        movie_genre_tv.text = genre
        movie_rating_tv.text = rating
        movie_duration_tv.text = duration
        movie_release_date_tv.text = releaseDate
        movie_synopsis_tv.text = synopsis

        cast_rv.layoutManager = LinearLayoutManager(this)
        cast_rv.adapter = CastAdapter(cast)

        loader_view.visibility = View.GONE
    }

    override fun displayErrorMessage() {
        loader_view.visibility = View.GONE
    }
}
