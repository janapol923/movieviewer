package com.insta.japt.movieviewer.data.model

import com.google.gson.annotations.SerializedName

data class MovieScheduleResponse(
    val dates: List<Dates>,
    val cinemas: List<Cinemas>,
    val times: List<Times>
)

data class Dates(
    override val id: String,
    override val label: String,
    val date: String
) : SpinnerModel

data class Cinemas(
    val parent: String,
    val cinemas: List<CinemaDetails>
)

data class Times(
    val parent: String,
    val times: List<TimeDetails>
)

data class CinemaDetails(
    override val id: String,
    @SerializedName("cinema_id")
    val cinemaId: String,
    override val label: String
) : SpinnerModel

data class TimeDetails(
    override val id: String,
    override val label: String,
    @SerializedName("schedule_id")
    val scheduleId: String,
    @SerializedName("popcorn_price")
    val popcornPrice: String,
    @SerializedName("popcorn_label")
    val popcornLabel: String,
    @SerializedName("seating_type")
    val seatingType: String,
    val price: String,
    val variant: String
) : SpinnerModel

interface SpinnerModel {
    val id: String
    val label: String
}