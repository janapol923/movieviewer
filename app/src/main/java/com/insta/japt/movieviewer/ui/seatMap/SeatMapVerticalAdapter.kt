package com.insta.japt.movieviewer.ui.seatMap

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.insta.japt.movieviewer.R
import com.insta.japt.movieviewer.data.model.Available
import kotlinx.android.synthetic.main.layout_seatmap_vertical.view.*

class SeatMapVerticalAdapter(
    private val seatmap: List<List<String>>,
    private val availableSeats: Available,
    private val listener: SeatMapHorizontalAdapter.OnSeatSelectedListener
) :
    RecyclerView.Adapter<SeatMapVerticalAdapter.ViewHolder>() {

    lateinit var mContext: Context

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        mContext = parent.context
        val itemView = LayoutInflater.from(mContext)
            .inflate(R.layout.layout_seatmap_vertical, parent, false)
        return ViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val linearLayoutManager = LinearLayoutManager(mContext)
        linearLayoutManager.orientation = LinearLayoutManager.HORIZONTAL
        holder.seatMapRecyclerView.layoutManager = linearLayoutManager
        holder.seatMapRecyclerView.adapter =
            SeatMapHorizontalAdapter(seatmap[position], availableSeats.seats, listener)

        holder.seatMapRecyclerView.setHasFixedSize(true)
        holder.seatMapRecyclerView.isNestedScrollingEnabled = false

        val seatMapLetter = seatmap[position][0].substring(0, 1)
        holder.seatMapRowLetterLeft.text = seatMapLetter
        holder.seatMapRowLetterRight.text = seatMapLetter
    }

    override fun getItemCount(): Int {
        return seatmap.size
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val seatMapRecyclerView: RecyclerView = itemView.seat_map_horizontal_rv
        val seatMapRowLetterLeft: TextView = itemView.seat_map_row_letter_left
        val seatMapRowLetterRight: TextView = itemView.seat_map_row_letter_right
    }
}