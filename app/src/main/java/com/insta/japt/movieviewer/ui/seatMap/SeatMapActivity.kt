package com.insta.japt.movieviewer.ui.seatMap

import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.insta.japt.movieviewer.R
import com.insta.japt.movieviewer.data.model.Available
import com.insta.japt.movieviewer.data.model.Cinemas
import com.insta.japt.movieviewer.data.model.Dates
import com.insta.japt.movieviewer.data.model.Times
import dagger.android.support.DaggerAppCompatActivity
import kotlinx.android.synthetic.main.activity_seat_map.*
import javax.inject.Inject

class SeatMapActivity : DaggerAppCompatActivity(),
    SeatMapContract.View,
    SeatMapHorizontalAdapter.OnSeatSelectedListener,
    AdapterView.OnItemSelectedListener {

    @Inject
    lateinit var mPresenter: SeatMapContract.Presenter

    private var mSeatNumberList: List<String> = ArrayList()
    private lateinit var mSelectedSeatAdapter: SelectedSeatAdapter

    private lateinit var mCinemas: List<Cinemas>
    private lateinit var mTimeList: List<Times>
    private var mSelectedTimeIndex: Int = -1

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_seat_map)

        setupSelectedSeatsRecyclerView(false)
        mPresenter.requestSeatMapDetails()
        mPresenter.requestScheduleDetails()
    }

    override fun onDestroy() {
        mPresenter.onDestroy()
        super.onDestroy()
    }

    override fun displaySeatMapDetails(seatmap: List<List<String>>, availableSeats: Available) {
        seat_map_vertical_rv.layoutManager = LinearLayoutManager(this)
        seat_map_vertical_rv.adapter = SeatMapVerticalAdapter(seatmap, availableSeats, this)

        seat_map_vertical_rv.setHasFixedSize(true)
        seat_map_vertical_rv.isNestedScrollingEnabled = false
    }

    override fun displayScheduleDetails(
        dates: List<Dates>,
        cinemas: List<Cinemas>,
        timeList: List<Times>
    ) {
        populateDateSpinner(dates)
        mCinemas = cinemas
        mTimeList = timeList
    }

    override fun displayTotalPrice(price: String) {
        total_price_tv.text = price
    }

    override fun displayErrorMessage() {
    }

    override fun onSeatSelected(seatNumber: String) {
        if (mSeatNumberList.size < 10) {
            mSeatNumberList = mSeatNumberList + seatNumber
            if (mSeatNumberList.size > 5) {
                setupSelectedSeatsRecyclerView(true)
            } else {
                mSelectedSeatAdapter.updateSelectedSeats(mSeatNumberList)
            }
            mPresenter.addSeat()
        } else {
            Toast.makeText(this, "Only 10 seats can be selected", Toast.LENGTH_SHORT).show()
        }
    }

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        val parentView: View? = view?.parent as View
        val itemTextView: TextView? = parentView?.findViewById(R.id.spinner_item)

        val tag: String = itemTextView?.tag.toString()
        when (tag.substring(tag.indexOf(",") + 1)) {
            getString(R.string.schedule_type_date) -> populateCinemaSpinner(
                tag.substring(
                    0,
                    tag.indexOf(",")
                )
            )
            getString(R.string.schedule_type_cinema) -> populateTimeListSpinner(
                tag.substring(
                    0,
                    tag.indexOf(",")
                )
            )
            getString(R.string.schedule_type_time) -> {
                mPresenter.updatePricePerSeat(mTimeList[mSelectedTimeIndex].times[position].price.toFloat())
            }
        }
    }

    override fun onNothingSelected(p0: AdapterView<*>?) {
    }

    private fun setupSelectedSeatsRecyclerView(gridLayout: Boolean) {
        selected_seats_rv.layoutManager = if (gridLayout) {
            val grid: Int = mSeatNumberList.size / 2
            GridLayoutManager(this, if (grid % 2 == 0) grid - 1 else grid)
        } else {
            GridLayoutManager(this, 5)
        }

        mSelectedSeatAdapter = SelectedSeatAdapter(mSeatNumberList)
        selected_seats_rv.adapter = mSelectedSeatAdapter
    }

    private fun populateDateSpinner(dates: List<Dates>) {
        val dateAdapter =
            ScheduleSpinnerAdapter(
                this,
                R.layout.layout_spinner,
                R.id.spinner_item,
                dates,
                getString(R.string.schedule_type_date)
            )
        date_spinner.adapter = dateAdapter
        date_spinner.onItemSelectedListener = this
    }

    private fun populateCinemaSpinner(id: String) {
        var selectedCinemaIndex = -1
        for ((index, cinema) in mCinemas.withIndex()) {
            if (cinema.parent == id) {
                selectedCinemaIndex = index
            }
        }

        cinema_spinner.onItemSelectedListener = this
        cinema_spinner.adapter = if (selectedCinemaIndex != -1) {
            ScheduleSpinnerAdapter(
                this,
                R.layout.layout_spinner,
                R.id.spinner_item,
                mCinemas[selectedCinemaIndex].cinemas,
                getString(R.string.schedule_type_cinema)
            )
        } else {
            populateTimeListSpinner("-1")
            ScheduleSpinnerAdapter(
                this,
                R.layout.layout_spinner,
                R.id.spinner_item,
                ArrayList(),
                getString(R.string.schedule_type_cinema)
            )
        }
    }

    private fun populateTimeListSpinner(id: String) {
        mSelectedTimeIndex = -1
        for ((index, time) in mTimeList.withIndex()) {
            if (time.parent == id) {
                mSelectedTimeIndex = index
            }
        }

        time_spinner.onItemSelectedListener = this
        time_spinner.adapter = if (mSelectedTimeIndex != -1) {
            ScheduleSpinnerAdapter(
                this,
                R.layout.layout_spinner,
                R.id.spinner_item,
                mTimeList[mSelectedTimeIndex].times,
                getString(R.string.schedule_type_time)
            )
        } else {
            mPresenter.updatePricePerSeat(0f)
            ScheduleSpinnerAdapter(
                this,
                R.layout.layout_spinner,
                R.id.spinner_item,
                ArrayList(),
                getString(R.string.schedule_type_time)
            )
        }
    }
}
