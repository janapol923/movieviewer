package com.insta.japt.movieviewer.ui.seatMap

import com.insta.japt.movieviewer.data.model.Available
import com.insta.japt.movieviewer.data.model.Cinemas
import com.insta.japt.movieviewer.data.model.Dates
import com.insta.japt.movieviewer.data.model.Times

interface SeatMapContract {

    interface View {

        fun displaySeatMapDetails(seatmap: List<List<String>>, availableSeats: Available)

        fun displayScheduleDetails(
            dates: List<Dates>,
            cinemas: List<Cinemas>,
            timeList: List<Times>
        )

        fun displayTotalPrice(price: String)

        fun displayErrorMessage()
    }

    interface Presenter {

        fun requestSeatMapDetails()

        fun requestScheduleDetails()

        fun updatePricePerSeat(price: Float)

        fun addSeat()

        fun onDestroy()
    }
}