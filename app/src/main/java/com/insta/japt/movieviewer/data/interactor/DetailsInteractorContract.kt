package com.insta.japt.movieviewer.data.interactor

import com.insta.japt.movieviewer.data.model.MovieDetailsResponse
import io.reactivex.disposables.Disposable

interface DetailsInteractorContract {

    interface OnDetailsLoadedListener {

        fun onDetailsLoadSuccess(response: MovieDetailsResponse)

        fun onError(error: String)

        fun onFailed()
    }

    fun requestMovieDetails(listener: OnDetailsLoadedListener): Disposable
}