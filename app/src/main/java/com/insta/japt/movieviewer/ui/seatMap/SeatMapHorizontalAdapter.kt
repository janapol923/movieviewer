package com.insta.japt.movieviewer.ui.seatMap

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.insta.japt.movieviewer.R
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.observers.DisposableObserver
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.layout_seatmap_horizontal.view.*


class SeatMapHorizontalAdapter(
    private val seatMapHorizontal: List<String>,
    private val availableSeats: List<String>,
    private val listener: OnSeatSelectedListener
) :
    RecyclerView.Adapter<SeatMapHorizontalAdapter.ViewHolder>() {

    lateinit var mContext: Context

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        mContext = parent.context
        val itemView = LayoutInflater.from(mContext)
            .inflate(R.layout.layout_seatmap_horizontal, parent, false)
        return ViewHolder(itemView)
    }

    override fun getItemCount(): Int {
        return seatMapHorizontal.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        if (seatMapHorizontal[position].equals("a(30)", true)) {
            holder.seatView.visibility = View.GONE
        } else {
            holder.seatView.visibility = View.VISIBLE

            val observable = Observable.create<String> { subscriber ->
                for (item in availableSeats) {
                    if (seatMapHorizontal[position] == item) {
                        subscriber.onComplete()
                        break
                    } else {
                        subscriber.onNext(item)
                    }
                }
            }

            val disposable = observable
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(object : DisposableObserver<String>() {
                    override fun onComplete() {
                        holder.seatView.setBackgroundResource(R.drawable.button_seat_map_bg)
                        holder.seatView.isClickable = true
                        holder.seatView.isFocusable = true

                        holder.seatView.setOnClickListener {
                            holder.seatView.setBackgroundResource(R.color.seat_map_selected)
                            listener.onSeatSelected(seatMapHorizontal[position])
                        }
                    }

                    override fun onNext(item: String) {
                    }

                    override fun onError(error: Throwable) {
                    }
                })
        }
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val seatView: View = itemView.seat_view
    }

    interface OnSeatSelectedListener {
        fun onSeatSelected(seatNumber: String)
    }
}