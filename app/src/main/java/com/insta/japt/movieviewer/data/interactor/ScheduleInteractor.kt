package com.insta.japt.movieviewer.data.interactor

import com.insta.japt.movieviewer.data.model.MovieScheduleResponse
import com.insta.japt.movieviewer.data.rest.MovieViewerService
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.observers.DisposableSingleObserver
import io.reactivex.schedulers.Schedulers

class ScheduleInteractor(private val service: MovieViewerService) : ScheduleInteractorContract {

    override fun requestScheduleDetails(listener: ScheduleInteractorContract.OnLoadScheduleDetailsListener): Disposable {
        val single = service.getMovieScheduleDetails()
        val singleCached = single.cache()

        return singleCached.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeWith(object : DisposableSingleObserver<MovieScheduleResponse>() {

                override fun onSuccess(response: MovieScheduleResponse) {
                    listener.onScheduleDetailsLoadSuccess(response)
                }

                override fun onError(e: Throwable) {
                    listener.onFailed()
                }
            })
    }

}