package com.insta.japt.movieviewer.ui.details

interface DetailsContract {
    interface View {
        fun displayMovieDetails(
            backdropUrl: String,
            posterUrl: String,
            name: String,
            genre: String,
            rating: String,
            duration: String,
            releaseDate: String,
            synopsis: String,
            cast: List<String>
        )

        fun displayErrorMessage()
    }

    interface Presenter {

        fun requestMovieDetails()

        fun onDestroy()
    }

}