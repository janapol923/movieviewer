package com.insta.japt.movieviewer.ui.seatMap

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.insta.japt.movieviewer.R

class SelectedSeatAdapter(private var selectedSeatList: List<String>) :
    RecyclerView.Adapter<SelectedSeatAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val itemView = LayoutInflater.from(parent.context)
            .inflate(R.layout.layout_selected_seatmap, parent, false)
        return ViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.selectedSeatTextView.text = selectedSeatList[position]
    }

    override fun getItemCount(): Int {
        return selectedSeatList.size
    }

    fun updateSelectedSeats(selectedSeatList: List<String>) {
        this.selectedSeatList = selectedSeatList
        notifyDataSetChanged()
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val selectedSeatTextView: TextView = itemView.findViewById(R.id.selected_seat_tv)
    }
}